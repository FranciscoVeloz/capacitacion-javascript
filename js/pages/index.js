import { TaskItem } from "../components/TaskItem.js";
import { list, deleteData } from "../services/task.services.js";

//Dom elements
const taskList = document.getElementById("taskList");

//Getting and printing the data
const getData = async () => {
  const result = await list();
  if (!result.status) return alert(`Something went wrong! - ${result.message}`);

  taskList.innerHTML = "";
  result.data.map(
    (d) => (taskList.innerHTML += TaskItem(d.id, d.title, d.description))
  );
};

getData();

//Handle delete
window.deleteData = async (id) => {
  const res = confirm("Do you want to delete this task?");

  if (!res) return;

  const result = await deleteData(id);
  if (!result.status) return alert(`Something went wrong! - ${result.message}`);

  getData();
};
