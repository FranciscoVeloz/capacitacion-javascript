export const TaskItem = (id, title, description) => `
<div class="task">
  <div class="task-header">${title}</div>
  <div class="task-body">
    <p>${description}</p>
    
    <div class="task-actions">
      <a class="task-actions-btn" href="/form.html#${id}">Edit</a>
      <button class="task-actions-btn" onclick={deleteData(${id})}>Delete</button>
    </div>
  </div>
</div>`;
